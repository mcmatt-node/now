"use strict";

let moment = require('moment');

module.exports = customFormat => moment.utc().format(customFormat === undefined ? 'YYYY-MM-DD HH:mm:ss.SSS' : customFormat);
